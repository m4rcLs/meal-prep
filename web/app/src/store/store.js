import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        drawerOpen: true,
        meals: [],
    },
    getters: {
        getDrawerOpen(state) {
            return state.drawerOpen;
        },
        getMeals(state) {
            return state.meals;
        }
    },
    mutations: {
        toggleDrawer(state) {
            state.drawerOpen = !state.drawerOpen
            return;
        },
        updateMeals(state, meals) {
            state.meals = meals;
            return;
        },
    },
    actions: {
        reloadMeals(context) {
            axios.get("http://localhost:8000/meals").then((response) => {
                console.log(response.data.meals);
                context.commit("updateMeals", response.data.meals);
            });
        }
    }
})
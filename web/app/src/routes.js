import Home from './components/Home.vue';
import Meals from './components/Meals.vue';

export const routes = [
    {
        path: '',
        components: {
            default: Home,
        },
        name: 'home'
    },
    {
        path: '/meals',
        components: {
            default: Meals,
        },
        children: [
            // { path: '', component: UserStart },
            // {
            //     path: ':id', component: UserDetail, beforeEnter: (to, from, next) => {
            //         console.log('beforeEnter');
            //         // next(false) to throw back to from page
            //         // next('/') or next({ path: '/'}) to redirect to other page
            //         next();
            //     }
            // },
            // { path: ':id/edit', component: UserEdit, name: 'userEdit' },
        ],
    },
    { path: '*', redirect: '/' }
];
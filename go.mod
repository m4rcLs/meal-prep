module gitlab.com/m4rcLs/meal-prep

go 1.12

require (
	github.com/gofiber/cors v0.2.2
	github.com/gofiber/fiber v1.14.5
	github.com/gorilla/schema v1.2.0 // indirect
	github.com/jinzhu/gorm v1.9.14
	github.com/klauspost/compress v1.10.11 // indirect
	golang.org/x/sys v0.0.0-20200905004654-be1d3432aa8f // indirect
)

package main

import (
	"flag"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/m4rcLs/meal-prep/internal/database"
	"gitlab.com/m4rcLs/meal-prep/internal/log"
)

const defaultDbFile = "meals.db"

func main() {
	var (
		initFile string
		dbFile   string
		force    bool
	)
	flag.StringVar(&initFile, "init", "", "A json file to initialize the database")
	flag.StringVar(&dbFile, "db", defaultDbFile, "Path to the sqlite database file")
	flag.BoolVar(&force, "f", false, "force initialization from file")
	flag.Parse()

	db := database.NewDatabaseHandler(dbFile)
	db.Open()
	defer db.Close()

	if initFile != "" {
		db.InitFromFile(initFile, force)
	}

	log.PrettyLog(db.GetMeal(1))
}

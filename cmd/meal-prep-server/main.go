package main

import (
	"flag"

	"github.com/gofiber/cors"
	"github.com/gofiber/fiber"
	"gitlab.com/m4rcLs/meal-prep/internal/api"
	"gitlab.com/m4rcLs/meal-prep/internal/database"
)

const defaultDbFile = "meals.db"

func main() {
	var dbFile string

	flag.StringVar(&dbFile, "db", defaultDbFile, "Path to the sqlite database file")
	flag.Parse()

	db := database.NewDatabaseHandler(dbFile)
	db.InitDatabase()
	defer db.Close()

	app := fiber.New()
	app.Use(cors.New())
	api := api.New(app, db)
	api.SetupHandlers()

	api.Listen(8000)
}

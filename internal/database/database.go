package database

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/m4rcLs/meal-prep/internal/models"
)

// DatabaseHandler is handling database connection, queries, etc.
type DatabaseHandler struct {
	conn   *gorm.DB
	dbFile string
}

// NewDatabaseHandler creates a new DatabaseHandler
func NewDatabaseHandler(dbFile string) (db *DatabaseHandler) {
	if dbFile == "" {
		dbFile = "meals.db"
	}

	db = &DatabaseHandler{dbFile: dbFile}

	return db
}

func (db *DatabaseHandler) readInitFile(file string, force bool) (meals *models.Meals, err error) {
	meals = &models.Meals{}

	if _, err = os.Stat(file); os.IsNotExist(err) {
		log.Printf("%s does not exist. Cannot initialize from file!", file)
		return
	}

	jsonFile, err := os.Open(file)

	if err != nil {
		log.Printf("Could not open %s: %s", file, err.Error())
	}

	bytes, err := ioutil.ReadAll(jsonFile)

	if err != nil {
		log.Printf("Could not read %s: %s", file, err.Error())
	}

	meals.UnmarshalJSON(bytes)

	return
}

// InitFromFile reads a json file and inserts the data into a new database.
func (db *DatabaseHandler) InitFromFile(file string, force bool) {
	dbFileExistsAndIsNotEmpty := false
	if fileInfo, err := os.Stat(db.dbFile); err == nil && fileInfo.Size() > 0 {
		dbFileExistsAndIsNotEmpty = true
	}

	if dbFileExistsAndIsNotEmpty && !force {
		log.Printf("Database file %s already exists and is not empty. Use -f to force initialization from file!", db.dbFile)
		return
	}

	meals, err := db.readInitFile(file, force)

	if err != nil {
		return
	}

	if dbFileExistsAndIsNotEmpty {
		os.Remove(db.dbFile)
	}

	db.InitDatabase()
	fmt.Printf("%v", meals)

	meals = db.CreateMeals(*meals)
	// TODO: Implement inserts

	return
}

// InitDatabase opens up the database connection and migrates the schema.
func (db *DatabaseHandler) InitDatabase() (err error) {
	db.Open()
	db.migrateSchema(&models.FoodItem{})
	db.migrateSchema(&models.Ingredient{})
	db.migrateSchema(&models.Meal{})
	db.migrateSchema(&models.QuantityCategory{})

	return
}

func (db *DatabaseHandler) migrateSchema(schema interface{}) {
	db.conn.AutoMigrate(schema)
}

// Open opens up the database connection
func (db *DatabaseHandler) Open() {
	conn, err := gorm.Open("sqlite3", db.dbFile)
	if err != nil {
		log.Panicf("Could not open database connection: %s", err.Error())
		return
	}
	db.conn = conn
}

// Close wraps up work on the database and closes the connection.
func (db *DatabaseHandler) Close() {
	defer db.conn.Close()
}

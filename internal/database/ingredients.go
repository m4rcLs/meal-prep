package database

import "gitlab.com/m4rcLs/meal-prep/internal/models"

func (db *DatabaseHandler) CreateIngredient(ingredient models.Ingredient) *models.Ingredient {
	if !db.conn.NewRecord(ingredient) {
		return &ingredient
	}

	foodItem := ingredient.FoodItem

	if db.conn.NewRecord(foodItem) {
		foodItem = db.GetFoodItemByName(ingredient.FoodItem.Name)
		if db.conn.NewRecord(foodItem) {
			ingredient.FoodItem = db.CreateFoodItem(*foodItem)
		}
	}

	db.conn.Create(&ingredient)

	return &ingredient
}

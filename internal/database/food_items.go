package database

import "gitlab.com/m4rcLs/meal-prep/internal/models"

func (db *DatabaseHandler) CreateFoodItem(foodItem models.FoodItem) *models.FoodItem {
	if db.conn.NewRecord(foodItem) {
		db.conn.Create(&foodItem)
	}
	return &foodItem
}

// GetFoodItemByName retrieves a FoodItem by it's name.
// Should only find one FoodItem because of an unique constraint.
func (db *DatabaseHandler) GetFoodItemByName(name string) (foodItem *models.FoodItem) {
	foodItem = &models.FoodItem{}
	db.conn.Where("name = ?", name).First(&foodItem)
	return
}

package database

import (
	"gitlab.com/m4rcLs/meal-prep/internal/errors"
	"gitlab.com/m4rcLs/meal-prep/internal/models"
)

func (db *DatabaseHandler) CreateMeal(meal models.Meal) (*models.Meal, error) {

	if existingMeal := db.GetMealByName(meal.Name); existingMeal.ID != 0 {
		return nil, errors.NewRecordAlreadyExists("Meal", "name", meal.Name)
	}

	for _, ingredient := range meal.Ingredients {
		ingredient = db.CreateIngredient(*ingredient)
	}

	db.conn.Create(&meal)

	return &meal, nil
}

// func (db *DatabaseHandler) Update() {

// }

// func (db *DatabaseHandler) Delete() {

// }

// GetMeal returns the Meal for the given id.
func (db *DatabaseHandler) GetMeal(id int64) (meal *models.Meal) {
	meal = &models.Meal{}
	db.conn.Where("id = ?", id).First(&meal)
	return
}

// GetMealByName returns the Meal for the given name.
func (db *DatabaseHandler) GetMealByName(name string) (meal *models.Meal) {
	meal = &models.Meal{}
	db.conn.Where("name = ?", name).First(&meal)
	return
}

func (db *DatabaseHandler) CreateMeals(meals models.Meals) *models.Meals {
	for _, meal := range meals.Meals {
		meal, _ = db.CreateMeal(*meal)
	}

	return &meals
}

// GetMeals returns all existing meals.
func (db *DatabaseHandler) GetMeals() *models.Meals {
	meals := models.Meals{
		Meals: []*models.Meal{},
	}

	db.conn.Find(&meals.Meals)
	return &meals
}

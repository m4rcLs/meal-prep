package log

import (
	"encoding/json"
	"log"
)

func PrettyLog(object interface{}) {
	bytes, err := json.Marshal(object)

	if err != nil {
		log.Panicf("Could not marshal object: %s", err.Error())
	}

	log.Println(string(bytes))
}

package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gofiber/fiber"
	"gitlab.com/m4rcLs/meal-prep/internal/models"
)

func (api *API) sendError(c *fiber.Ctx, title string, status int, detail string) {
	problem := models.ProblemJSON{
		Title:  title,
		Status: status,
		Detail: detail,
	}
	problemJSON, _ := json.Marshal(problem)
	c.Set("Content-Type", "application/problem+json")
	c.Status(status).SendBytes(problemJSON)
}

func (api *API) sendConflictError(c *fiber.Ctx, detail string) {
	api.sendError(c, "Conflict", http.StatusConflict, detail)
}

func (api *API) sendNotFoundError(c *fiber.Ctx, detail string) {
	api.sendError(c, "Not Found", http.StatusNotFound, detail)
}

func (api *API) sendBadRequestError(c *fiber.Ctx, detail string) {
	api.sendError(c, "Bad Request", http.StatusBadRequest, detail)
}

func (api *API) getFoodItems(c *fiber.Ctx) {
	c.Send("getFoodItems")
}

func (api *API) getMealsWithFoodItem(c *fiber.Ctx) {
	c.Send("getMealsWithFoodItem")
}

func (api *API) getMeals(c *fiber.Ctx) {
	c.JSON(api.db.GetMeals())
}

func (api *API) addMeal(c *fiber.Ctx) {
	var meal *models.Meal
	err := c.BodyParser(&meal)

	if err != nil {
		log.Fatalf("Could not parse body on addMeal: %v", err)
	}

	meal, err = api.db.CreateMeal(*meal)

	if err != nil {
		api.sendConflictError(c, err.Error())
		return
	}

	c.JSON(meal)
}

func (api *API) getMeal(c *fiber.Ctx) {
	mealID, err := strconv.Atoi(c.Params("meal_id"))

	if err != nil {
		api.sendBadRequestError(c, "meal_id needs to be an integer")
		return
	}

	meal := api.db.GetMeal(int64(mealID))

	if meal.ID == 0 {
		api.sendNotFoundError(c, fmt.Sprintf("No meal found with id %d", mealID))
		return
	}

	c.JSON(meal)
}

func (api *API) updateMeal(c *fiber.Ctx) {
	c.Send("updateMeal")
}

func (api *API) updateIngredient(c *fiber.Ctx) {
	c.Send("updateIngredient")
}

func (api *API) deleteIngredient(c *fiber.Ctx) {
	c.Send("deleteIngredient")
}

func (api *API) getMealIngredients(c *fiber.Ctx) {
	c.Send("getMealIngredients")
}

func (api *API) addIngredientToMeal(c *fiber.Ctx) {
	c.Send("addIngredientToMeal")
}

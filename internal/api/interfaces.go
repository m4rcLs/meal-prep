package api

import (
	"crypto/tls"

	"github.com/gofiber/fiber"
	"gitlab.com/m4rcLs/meal-prep/internal/models"
)

// DatabaseHandler describes a handler for working with a database.
type DatabaseHandler interface {
	CreateMeal(meal models.Meal) (*models.Meal, error)
	GetMeal(id int64) *models.Meal
	GetMeals() *models.Meals
}

// App describes every necessary method for HTTP handling
// It will probably always be a fiber.App
type App interface {
	Get(string, ...func(*fiber.Ctx)) fiber.Router
	Post(string, ...func(*fiber.Ctx)) fiber.Router
	Put(string, ...func(*fiber.Ctx)) fiber.Router
	Delete(string, ...func(*fiber.Ctx)) fiber.Router
	Listen(interface{}, ...*tls.Config) error
}

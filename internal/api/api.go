package api

import (
	"log"
)

type API struct {
	app App
	db  DatabaseHandler
}

// New creates a new API.
func New(app App, db DatabaseHandler) *API {
	api := API{
		app: app,
		db:  db,
	}

	return &api
}

// Listen exposes the API on the given port.
func (api *API) Listen(port int) {
	api.app.Listen(port)
	return
}

// SetupHandlers sets up all the handlers for the REST API.
func (api *API) SetupHandlers() {
	log.Print("Setup API handlers...")

	app := api.app

	// TODO: Use Groups
	// FoodItems
	app.Get("/food-items", api.getFoodItems)
	app.Get("/food-items/:food_item_id/meals", api.getMealsWithFoodItem)

	// Meals
	app.Get("/meals", api.getMeals)
	app.Post("/meals", api.addMeal)
	app.Get("/meals/:meal_id", api.getMeal)
	app.Put("/meals/:meal_id", api.updateMeal)

	// Ingredients
	app.Get("/meals/:meal_id/ingredients", api.getMealIngredients)
	app.Post("/meals/:meal_id/ingredients", api.addIngredientToMeal)
	app.Put("/ingredients/:ingredient_id", api.updateIngredient)
	app.Delete("/ingredients/:ingredient_id", api.deleteIngredient)
}

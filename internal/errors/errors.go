package errors

import "fmt"

// DbFileMissing describes a missing sqlite db file.
type DbFileMissing struct {
	s      string
	dbFile string
}

// DbFileRequired describes a missing sqlite db file.
type DbFileRequired struct {
	s string
}

// RecordAlreadyExists describes that a record is already existing in the database.
type RecordAlreadyExists struct {
	s          string
	recordType string
	column     string
	value      interface{}
}

// NewDbFileRequired creates a DbFileRequired error.
func NewDbFileRequired() *DbFileMissing {
	return &DbFileMissing{
		s: "Database file is required",
	}
}

// NewDbFileMissing creates a DbFileMissing error.
func NewDbFileMissing(dbFile string) *DbFileMissing {
	return &DbFileMissing{
		s:      fmt.Sprintf("Database file %s is missing", dbFile),
		dbFile: dbFile,
	}
}

// NewRecordAlreadyExists creates a RecordAlreadyExists error.
func NewRecordAlreadyExists(recordType string, column string, value interface{}) *RecordAlreadyExists {
	return &RecordAlreadyExists{
		s:          fmt.Sprintf("%s with %s %v already exists in database.", recordType, column, value),
		recordType: recordType,
		column:     column,
		value:      value,
	}
}

func (d *DbFileMissing) Error() string {
	return d.s
}

func (d *DbFileRequired) Error() string {
	return d.s
}

func (d *RecordAlreadyExists) Error() string {
	return d.s
}

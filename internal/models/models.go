package models

import (
	"encoding/json"
	"time"
)

// ProblemJSON is the response schema for error responses.
type ProblemJSON struct {
	Title  string `json:"title"`
	Detail string `json:"detail"`
	Status int    `json:"status"`
}

// BaseModel definition
type BaseModel struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}

// QuantityCategory categorizes in which quantity the Ingredient is displayed. (e.g. Weight, Volume, Pieces)
type QuantityCategory struct {
	BaseModel
	Name   string `json:"name"`
	Symbol string `json:"symbol"`
}

// A FoodItem is any edible grocery.
type FoodItem struct {
	BaseModel
	Name  string `gorm:"unique;not null" json:"name"`
	Image string `json:"image,omitempty"`
}

// An Ingredient is a FoodItem used in a meal.
type Ingredient struct {
	BaseModel
	Quantity           uint `json:"quantity"`
	QuantityCategoryID uint `json:"-"`
	FoodItemID         uint `json:"-"`

	FoodItem         *FoodItem         `json:"food_item"`
	QuantityCategory *QuantityCategory `json:"quantity_category"`
}

// A Meal consists of prepared Ingredients.
type Meal struct {
	BaseModel
	Name        string        `gorm:"unique;not null" json:"name"`
	Image       string        `json:"image,omitempty"`
	Calories    int           `json:"calories,omitempty"`
	Ingredients []*Ingredient `json:"ingredients,omitempty"`
}

// Meals are many of Meal.
type Meals struct {
	Meals []*Meal `json:"meals"`
}

// UnmarshalJSON reads a byte array containg JSON and unmarshals it
// into the corresponding Meals struct.
func (meals *Meals) UnmarshalJSON(bytes []byte) (err error) {

	var tempInterface interface{}
	err = json.Unmarshal(bytes, &tempInterface)

	mealsArray := tempInterface.([]interface{})

	for _, meal := range mealsArray {
		mealMap := meal.(map[string]interface{})
		mealObj := Meal{
			Name:        mealMap["name"].(string),
			Calories:    int(mealMap["calories"].(float64)),
			Ingredients: []*Ingredient{},
		}

		ingredientsArray := mealMap["ingredients"].([]interface{})
		for _, ingredient := range ingredientsArray {
			ingredientMap := ingredient.(map[string]interface{})

			foodItem := FoodItem{
				Name: ingredientMap["name"].(string),
			}
			quantityCategory := QuantityCategory{
				Symbol: ingredientMap["symbol"].(string),
			}

			mealObj.Ingredients = append(mealObj.Ingredients, &Ingredient{
				FoodItem:         &foodItem,
				QuantityCategory: &quantityCategory,
				Quantity:         uint(ingredientMap["quantity"].(float64)),
			})
		}

		meals.Meals = append(meals.Meals, &mealObj)

	}

	return
}
